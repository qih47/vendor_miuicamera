#!/vendor/bin/sh
#
# A simple script that checks if the anx directories are present

DEVICE=$(getprop ro.product.device)

rm -rf /sdcard/.ANXCamera/cheatcodes/
mkdir -p /sdcard/.ANXCamera/cheatcodes/
cp -R /system/etc/ANXCamera/cheatcodes/*${DEVICE}* /sdcard/.ANXCamera/cheatcodes/
rm -rf /sdcard/.ANXCamera/cheatcodes_reference/
mkdir -p /sdcard/.ANXCamera/cheatcodes_reference/
cp -R /system/etc/ANXCamera/cheatcodes/*${DEVICE}* /sdcard/.ANXCamera/cheatcodes_reference/
rm -rf /sdcard/.ANXCamera/features/
mkdir -p /sdcard/.ANXCamera/features/
cp -R /system/etc/device_features/*${DEVICE}* /sdcard/.ANXCamera/features/
rm -rf /sdcard/.ANXCamera/features_reference/
mkdir -p /sdcard/.ANXCamera/features_reference/
cp -R /system/etc/device_features/*${DEVICE}* /sdcard/.ANXCamera/features_reference/
rm -rf /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes/
mkdir -p /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes/
cp -R /system/etc/ANXCamera/cheatcodes/*${DEVICE}* /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes/
rm -rf /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes_reference/
mkdir -p /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes_reference/
cp -R /system/etc/ANXCamera/cheatcodes/*${DEVICE}* /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/cheatcodes_reference/
rm -rf /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features/
mkdir -p /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features/
cp -R /system/etc/device_features/*${DEVICE}* /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features/
rm -rf /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features_reference/
mkdir -p /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features_reference/
cp -R /system/etc/device_features/*${DEVICE}* /sdcard/Android/data/com.android.camera/sdcard/.ANXCamera/features_reference/
exit 0

if [ $DEVICE = platina ]
	then
		echo "Moving Cheatcodes for $DEVICE......!!! "
		rm -rf /sdcard/.ANXCamera/cheatcodes/
		mkdir -p /sdcard/.ANXCamera/cheatcodes/
		cp -R /patch/cheatcodes/Platina/*${DEVICE}* /sdcard/.ANXCamera/cheatcodes/
	else
		echo "Moving Cheatcodes for $DEVICE......!!! "
		rm -rf /sdcard/.ANXCamera/cheatcodes/
		mkdir -p /sdcard/.ANXCamera/cheatcodes/
		cp -R /patch/cheatcodes/Sirius/*${DEVICE}* /sdcard/.ANXCamera/cheatcodes/
	exit 0
fi

cp -R /patch/lib/*${DEVICE}*/system
cp -R /patch/lib64/*${DEVICE}*/system
exit 0



